# String Detector Kit
A framework that help you extract a given string to some informations like: emoticon, mention name, links, etc.
You can easily create your own detector to extract email address, phone number, float number, or address.

This framework is written by Swift 2.0, Xcode 7 Beta 2
## Add to your project
1. Clone the repository:

  ```
  git clone https://bitbucket.org/horse304/string-detector-kit.git
  ```

1. Drag `StringDetectorKit.xcodeproj` into your project
2. Add `StringDetectorKit.framework`


## Features
* Extract emoticons with format `(emoticon)`
* Extract mention name with format `@name`
* Extract links using this regular expression: http://regexr.com/39nr7

## Samples
```

import StringDetectorKit

var detector = StringDetector()
do {
    try detector.addHandler(EmojiHandler())
    try detector.addHandler(MentionHandler())
    try detector.addHandler(LinkHandler())
    detector.parse("@dato (emoji) http://google.com") { (jsonString, jsonObject, error) -> Void in
        if error == nil {
            print(jsonObject, appendNewLine: true)
        }
    }
} catch let error {
    //Handle error here
}
```