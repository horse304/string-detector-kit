//
//  EmojiHandler.swift
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import Foundation

/**
For this exercise, you only need to consider 'custom' emoticons which are **alphanumeric strings, no longer than 15 characters, contained in parenthesis**. You can assume that anything matching this format is an emoticon. [https://www.hipchat.com/emoticons](https://www.hipchat.com/emoticons)

- Note:
For alphanumeric strings, I used "\w" to represent alphanumeric character. There is a lot of inconsistency about which characters are actually included. Letters and digits from alphabetic scripts and ideographs are  included.
*/
public class EmojiHandler: RegexHandler {
    public convenience init() throws {
        do {
            try self.init(maxLength: 15)
        } catch let error {
            throw error
        }
    }
    
    public convenience init(maxLength: Int) throws {
        do {
            try self.init(regexPattern: "\\(\\w{1,\(maxLength)}\\)")
        } catch let error {
            throw error
        }
    }
    
    public override var detectorIdentifier: String { return "emoticons" }
    public override var ResultType: _RegexMatchedResult.Type { return EmojiMatchedResult.self }
}

public struct EmojiMatchedResult: _RegexMatchedResult {
    public init(matchedString: String) throws {
        self.matchedString = matchedString
        if self.matchedString.characters.count > 2 {
            self.emoji = matchedString.substringWithRange(Range<String.Index>(start: matchedString.startIndex.successor(), end: matchedString.endIndex.predecessor()))
        } else {
            self.emoji = ""
        }
    }
    public var emoji: String
    public var matchedString: String
    
    public func toJSONObject() -> AnyObject {
        return emoji
    }
}