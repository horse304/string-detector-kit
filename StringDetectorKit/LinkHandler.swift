//
//  LinkHandler.swift
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import Foundation

/**
For URL matching, I used this regular expression [http://regexr.com/39nr7](http://regexr.com/39nr7)
*/
public class LinkHandler: RegexHandler {
    
    public convenience init() throws {
        do {
            try self.init(regexPattern: "[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)")
            self.isResultCaseInsensitive = false
        } catch let error {
            throw error
        }
    }
    
    public override var detectorIdentifier: String { return "links" }
    public override var ResultType: _RegexMatchedResult.Type { return LinkMatchedResult.self }
}

public struct LinkMatchedResult: _RegexMatchedResult {
    public init(matchedString: String) throws {
        do {
            self.matchedString = matchedString
            if self.matchedString.characters.count > 1 {
                self.url = matchedString
            } else {
                self.url = ""
            }
            
            try self.title = self.getTitle()
        } catch let error {
            throw error
        }
    }
    public var url: String
    public var title: String?
    public var matchedString: String
    
    public func toJSONObject() -> AnyObject {
        let title = self.title != nil ? self.title! : ""
        return [
            "url":self.url,
            "title":title
        ]
    }
    
    ///This function need to improve later
    private func getTitle() throws -> String? {
        do {
            //Check if url don't have scheme, will add http scheme automatically
            var validUrl: NSURL? = nil
            if let urlComponents = NSURLComponents(string: self.url) {
                if urlComponents.scheme == nil {
                    urlComponents.scheme = "http"
                }
                
                validUrl = urlComponents.URL
            }
            
            if let url = validUrl {
                let urlRequest = NSURLRequest(URL: url, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringCacheData, timeoutInterval: 2.0)
                //Loading html source code from url
                if let pageData = try NSURLSession.sharedSession().sendSynchronousRequest(urlRequest) {
                    if let pageSourceCode = NSString(data: pageData, encoding: NSUTF8StringEncoding) as? String {
                        
                        //Use regular expression to extract title information
                        let regularExpression = try NSRegularExpression(pattern: "<title[^>]*>(.*?)</title>", options: NSRegularExpressionOptions.CaseInsensitive)
                        let matchingResults = regularExpression.firstMatchInString(pageSourceCode, options: NSMatchingOptions(), range: NSMakeRange(0, pageSourceCode.characters.count))
                        //Get the capture group's range
                        if let firstMatchRange = matchingResults?.rangeAtIndex(1) {
                            let titleString = pageSourceCode.substringWithRange(Range<String.Index>(start: advance(pageSourceCode.startIndex, firstMatchRange.location), end: advance(pageSourceCode.startIndex, firstMatchRange.location + firstMatchRange.length)))
                            return titleString
                        }
                    }
                }
                
                return nil
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
}

extension NSURLSession {
    //Because StringDetector are not supporting async pattern, so we have to make this function to load a request synchronously
    func sendSynchronousRequest(request: NSURLRequest) throws -> NSData? {
        let semaphore = dispatch_semaphore_create(0)
        var requestData: NSData?
        var requestError: NSError?
        
        let task = self.dataTaskWithRequest(request) { (pageData, response, error) -> Void in
            requestData = pageData
            requestError = error
            dispatch_semaphore_signal(semaphore)
        }
        if task != nil {
            task?.resume()
            
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        }
        
        if requestError != nil {
            throw requestError!
        } else {
            return requestData
        }
    }
}