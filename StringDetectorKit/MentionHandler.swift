//
//  MentionHandler.swift
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import Foundation

/** Always starts with an '@' and ends when hitting a non-word character.
*/
public class MentionHandler: RegexHandler {
    
    public convenience init() throws {
        do {
            try self.init(regexPattern: "\\@(\\w+)")
        } catch let error {
            throw error
        }
    }
    
    public override var detectorIdentifier: String { return "mentions" }
    public override var ResultType: _RegexMatchedResult.Type { return MentionMatchedResult.self }
}

public struct MentionMatchedResult: _RegexMatchedResult {
    public init(matchedString: String) throws {
        self.matchedString = matchedString
        if self.matchedString.characters.count > 1 {
            self.mentionName = matchedString.substringWithRange(Range<String.Index>(start: matchedString.startIndex.successor(), end: matchedString.endIndex))
        } else {
            self.mentionName = ""
        }
    }
    public var mentionName: String
    public var matchedString: String
    
    public func toJSONObject() -> AnyObject {
        return self.mentionName
    }
}