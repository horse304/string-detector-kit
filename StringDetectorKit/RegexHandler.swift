//
//  RegexHandler.swift
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/26/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import Foundation

public enum RegexHandlerError: ErrorType {
    case FailedCreatingRegularExpression
}

public class RegexHandler: DetectorHandler {
    /** Intialize a Regular Expression Handler with a pattern string. By default, the regular expression will initialized with options NSRegularExpressionOptions.CaseInsensitive
        
    If you want intialize with a custom regular expression, please see **init(regularExpression: NSRegularExpression)**
    */
    public convenience init(regexPattern: String) throws {
        do {
            let regularExpression = try NSRegularExpression(pattern: regexPattern, options: NSRegularExpressionOptions.CaseInsensitive)
            self.init(regularExpression: regularExpression)
        } catch let error as NSError {
            throw error
        }
    }
    
    /** Intialize a Regular Expression Handler with a NSRegularExpression
    - Parameter regularExpression: your own regular expression
    */
    public init(regularExpression: NSRegularExpression) {
        self.regularExpression = regularExpression
    }
    
    ///A handler's detectorIdentifier, every handler should have a unique detectorIdentifier. For subclass, should override this variable.
    public var detectorIdentifier: String { return "regex" }
    public private(set) var regularExpression: NSRegularExpression?
    ///The result type used for intializing a dynamic type of MatchedResult. Subclass can override this variable to support it's associated matched result.
    public var ResultType: _RegexMatchedResult.Type { return RegexMatchedResult.self }
    ///By default, this variable is **true**
    public var isResultCaseInsensitive: Bool = true
    
    public func handle(string: String) throws -> [MatchedResult]? {
        if let regularExpression = self.regularExpression {
            let matchedResultsSet = NSMutableOrderedSet() //Because Swift don't support Ordered Set, we have to use NSMutableOrderedSet here
            
            let wordLength = string.characters.count
            regularExpression.enumerateMatchesInString(string, options: NSMatchingOptions.ReportCompletion, range: NSRange(location: 0, length: wordLength), usingBlock: { (result, flags, stop) -> Void in
                if let result = result {
                    let resultRange = result.range
                    
                    //Get the matched string and add to the ordered set
                    let resultString = string.substringWithRange(Range<String.Index>(start: advance(string.startIndex, resultRange.location), end: advance(string.startIndex, resultRange.location + resultRange.length)))
                    
                    if self.isResultCaseInsensitive {
                        matchedResultsSet.addObject(resultString.lowercaseString as NSString)
                    } else {
                        matchedResultsSet.addObject(resultString as NSString)
                    }
                }
            })
            
            //Mapping all matched strings to MatchedResult
            do {
                var results = [MatchedResult]()
                for matchedString in matchedResultsSet {
                    let matchedResult = try self.ResultType.init(matchedString: matchedString as! String)
                    results.append(matchedResult)
                }
                return results
            } catch let error {
                throw error
            }
        } else {
            throw RegexHandlerError.FailedCreatingRegularExpression
        }
    }
}

public protocol _RegexMatchedResult: MatchedResult {
    init(matchedString: String) throws
    var matchedString: String {get}
}
public struct RegexMatchedResult: _RegexMatchedResult {
    public init(matchedString: String) throws {
        self.matchedString = matchedString
    }
    
    public var matchedString: String
    
    public func toJSONObject() -> AnyObject {
        return matchedString
    }
}