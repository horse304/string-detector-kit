//
//  StringDetector.swift
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import Foundation

public enum StringDetectorError: ErrorType {
    case NotHaveAnyHandler
    case NotAValidJsonObject
}
public class StringDetector: StringDetectorProtocol {
    //MARK: - Initialize functions
    /// By default we intialize with the global serial queue
    public convenience required init() {
        let globalQueue = NSOperationQueue()
        globalQueue.underlyingQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        self.init(queue: globalQueue)
    }
    
    /// Initialize with your own queue.
    public required init(queue: NSOperationQueue) {
        self.queue = queue
        self.allHandlers = [DetectorHandler]()
    }
    
    //MARK: - Properties
    public private(set) var queue: NSOperationQueue
    public private(set) var allHandlers: [DetectorHandler]
    
    //MARK: - Functions
    public func addHandler(handler: DetectorHandler) {
        self.allHandlers.append(handler)
    }
    public func addHandlers(handlers: [DetectorHandler]) {
        self.allHandlers += handlers
    }
    public func removeHandler<T: DetectorHandler where T: Equatable>(handler: T) {
        var foundIndex = -1
        for (index,addedHandler) in self.allHandlers.enumerate() {
            if (addedHandler as? T) == handler {
                foundIndex = index
                break
            }
        }
        
        if foundIndex != -1 {
            self.allHandlers.removeAtIndex(foundIndex)
        }
    }
    public func removeHandlers<T: DetectorHandler where T: Equatable>(handlers: [T]) {
        for handler in handlers {
            self.removeHandler(handler)
        }
    }
    public func removeAllHandlers() {
        self.allHandlers.removeAll()
    }
    
    /**
    This method create a NSOperation to provide a simple cancelable asynchronous interface.
     
    NSOperation objects are always enqueued immediately.
    
    - Parameter string: the given string
    - Parameter completionBlock: The callback block when parsing completed. **jsonString**: the json string is serialized from jsonObject. **jsonObject**: the result was detected and combinded from all handlers.
    - Returns: a NSOperation to provide cancelable asynchronous interface.
    */
    public func parse(string: String, completionBlock:((jsonString: String?, jsonObject: [String: [AnyObject]]?, error: ErrorType?)->Void)) -> NSOperation {
        //Create the operation. We will combine results from all handlers and callback to completionBlock
        let blockOperation = NSBlockOperation {[weak self] () -> Void in
            var allResults = [String: [AnyObject]]()
            
            //Loop through all handlers
            do {
                if let allHandlers = self?.allHandlers {
                    for handler in allHandlers {
                        let matchedItems = try handler.handle(string)
                        if let matchedItems = matchedItems where matchedItems.count > 0 {
                            if allResults[handler.detectorIdentifier] != nil {
                                allResults[handler.detectorIdentifier]! += matchedItems.map{$0.toJSONObject() as AnyObject}
                            } else {
                                allResults[handler.detectorIdentifier] = matchedItems.map{$0.toJSONObject() as AnyObject}
                            }
                        }
                    }
                    
                    completionBlock(jsonString: try self?.serializeToJSON(allResults), jsonObject: allResults, error: nil)
                } else {
                    completionBlock(jsonString: nil, jsonObject: nil, error: StringDetectorError.NotHaveAnyHandler)
                }
            } catch let error {
                completionBlock(jsonString: nil, jsonObject: nil, error: error)
            }
        }
        
        //Enqueue the operation
        self.queue.addOperation(blockOperation)
        
        return blockOperation
    }
    
    private func serializeToJSON(allResults: [String: [AnyObject]]) throws -> String? {
        if NSJSONSerialization.isValidJSONObject(allResults) {
            do {
                let data = try NSJSONSerialization.dataWithJSONObject(allResults, options: NSJSONWritingOptions())
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding) as? String {
                    return string
                }
                return nil
            } catch let error {
                throw error
            }
        } else {
            throw StringDetectorError.NotAValidJsonObject
        }
    }
}