//
//  StringDetectorKit.h
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for StringDetectorKit.
FOUNDATION_EXPORT double StringDetectorKitVersionNumber;

//! Project version string for StringDetectorKit.
FOUNDATION_EXPORT const unsigned char StringDetectorKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StringDetectorKit/PublicHeader.h>


