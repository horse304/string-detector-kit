//
//  StringDetectorProtocol.swift
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import Foundation

///A detector handler
public protocol DetectorHandler {
    var detectorIdentifier: String {get}
    
    ///Currently, supported synchronous pattern only
    func handle(word: String) throws -> [MatchedResult]?
}

public protocol MatchedResult {
    ///Override this function to convert MatchedResult value to JSON object
    func toJSONObject() -> AnyObject
}

public protocol StringDetectorProtocol {
    /// Initialize with your own queue
    init(queue: NSOperationQueue)
    
    ///The queue is used to executing parsing operations
    var queue: NSOperationQueue {get}
    ///All the added handler
    var allHandlers: [DetectorHandler] {get}
    
    func addHandler(handler: DetectorHandler)
    func addHandlers(handlers: [DetectorHandler])
    func removeHandler<T: DetectorHandler where T: Equatable>(handler: T)
    func removeHandlers<T: DetectorHandler where T: Equatable>(handlers: [T])
    func removeAllHandlers()
    
    /**
    This method create a NSOperation to provide a simple cancelable asynchronous interface.
     
    NSOperation objects are always enqueued immediately.
    
    #### Implementation
    Should call to every handler to handle the given string. And combine all the results to a json object as format below:
    
    ```json
    {
        "detectorIdentifier 1": [
            "Result 1",
            "Result 2"
        ],
        "detectorIdentifier 2": [
            { 
                "key 1": "value 1",
                "key 2": "value 2"
            }
        ]
    }
    
    */
    func parse(string: String, completionBlock:((jsonString: String?, jsonObject: [String: [AnyObject]]?, error: ErrorType?)->Void)) -> NSOperation
}