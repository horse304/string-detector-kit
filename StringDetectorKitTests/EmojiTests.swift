//
//  EmojiTests.swift
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import XCTest
import StringDetectorKit

class EmojiTests: StringDetectorKitTests {
    
    //(coffee)(beer)
    func testMultipleEmojiInOneWord() {
        self.testParseString("Good morning! (megusta) (coffee)(beer)", expectedJSONObject: [
            "emoticons":[
                "megusta",
                "coffee",
                "beer"
            ]
        ])
    }
    
    //(beer)(beer) (beer)
    func testSameEmojiAppearsMultipleTimes() {
        self.testParseString("(beer)(beer) (beer)", expectedJSONObject: [
            "emoticons":[
                "beer"
            ]
        ])
    }
    
    //(!) (@) ((a)) ($)(á)
    func testAlphabetNumericCharacters() {
        var testString = "(!)(@)(.a)($)"
        
        let expectation = self.expectationWithDescription("Parsing success")
        detector.parse(testString) { (jsonString, jsonObject, error) -> Void in
            XCTAssertTrue(error == nil)
            XCTAssertTrue(jsonString == "{}")
            XCTAssertTrue(jsonObject!.count == 0)
        }
        
        //For this exercise, you only need to consider 'custom' emoticons which are alphanumeric strings. For more detailed: alphanumeric characters included unicode characters, numeric, underscore
        testString = "(abcdefghi) (jklmnopqrs) (tuvxyz) (hello_boy) (0123456789) (áàảã)"
        detector.parse(testString) { (jsonString, jsonObject, error) -> Void in
            XCTAssertTrue(error == nil)
            XCTAssertTrue(jsonString == "{\"emoticons\":[\"abcdefghi\",\"jklmnopqrs\",\"tuvxyz\",\"hello_boy\",\"0123456789\",\"áàảã\"]}")
            XCTAssertTrue(jsonObject! == [
                "emoticons":[
                    "abcdefghi",
                    "jklmnopqrs",
                    "tuvxyz",
                    "hello_boy",
                    "0123456789",
                    "áàảã"
                ]
            ])
            
            expectation.fulfill()
        }
        
        self.waitForExpectationsWithTimeout(1.0, handler: nil)
    }
    
    func testEmojiMaxLength() {
        let maxLength = 15
        let testSuccessString = String(count: maxLength, repeatedValue: Character("ả"))
        let testFailedString = String(count: maxLength+1, repeatedValue: Character("a"))
        
        let expectation = self.expectationWithDescription("Parsing success")
        //The parse result should empty because the given emoji length > maxLength
        detector.parse("("+testFailedString+")") { (jsonString, jsonObject, error) -> Void in
            XCTAssertTrue(error == nil)
            XCTAssertTrue(jsonObject?.count == 0)
        }
        
        //given_emoji_length = maxLength, this parse should have 1 emoticon
        detector.parse("("+testSuccessString+")") { (jsonString, jsonObject, error) -> Void in
            XCTAssertTrue(error == nil)
            XCTAssertEqual(jsonObject!,[
                "emoticons":[
                    testSuccessString
                ]
            ])
            
            expectation.fulfill()
        }
        
        
        self.waitForExpectationsWithTimeout(1.0, handler: nil)
    }
}
