//
//  LinkTests.swift
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import XCTest

class LinkTests: StringDetectorKitTests {
    
    //http://regexr.com/39nr7
    func testLink1() {
        self.testParseString("Welcome to RegExr 0.3b, an intuitive tool for learning, writing, and testing Regular Expressions. Key features include: http://www.google.com * real time results: shows results as you type * code hinting: roll over your expression to see info on specific elements * detailed results: roll over a match to see details & view group info below * built in regex guide: double click entries to insert them into your expression * online & desktop: regexr.com or download the desktop version for Mac, Windows, or Linux * save your expressions: My Saved expressions are saved locally * search Comm https://google.us.edi?34535/534534?dfg=g&fg unity expressions and add your own * create Share Links to send your expressions to co-workers or link to them on Twitter or your blog [ex. http://RegExr.com?2rjl6] Built by gskinner.com with Flex 3  and Spelling Plus Library for text highlighting [gskinner.com/products/spl].", expectedJSONObject: [
            "links":[
                [
                    "url":"http://www.google.com",
                    "title":"Google"
                ],
                [
                    "url":"regexr.com",
                    "title":"RegExr: Learn, Build, & Test RegEx"
                ],
                [
                    "url":"https://google.us.edi?34535/534534?dfg=g&fg",
                    "title":""
                ],
                [
                    "url":"http://RegExr.com?2rjl6",
                    "title":"RegExr: Learn, Build, & Test RegEx"
                ],
                [
                    "url":"gskinner.com",
                    "title":"gskinner"
                ],
                [
                    "url":"gskinner.com/products/spl",
                    "title":"403 Forbidden"
                ]
            ]
        ], timeout: 10.0)
    }
    
    //Olympics are starting soon: http://nbcolympics.com/
    func testLink2() {
        self.testParseString("Olympics are starting soon: http://nbcolympics.com/", expectedJSONObject: [
            "links":[
                [
                    "url":"http://nbcolympics.com/",
                    "title":"NBC Olympics | Home of the 2016 Olympic Games in Rio"
                ]
            ]
        ])
    }
    
}
