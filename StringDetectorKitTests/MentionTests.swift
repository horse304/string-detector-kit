//
//  MentionTests.swift
//  StringDetectorKit
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import XCTest

class MentionTests: StringDetectorKitTests {
    
    //@dat, what are you talking, @dat?
    func testSameMentionNameAppearMultipleTimes() {
        self.testParseString("@dat, what are you talking, @dat?", expectedJSONObject: [
            "mentions":[
                "dat"
            ]
        ])
    }
    
    //@DatNguyen, what are you talking, @datnguyen?
    func testCaseInsensitive() {
        self.testParseString("@DatNguyen, what are you talking, @datnguyen?", expectedJSONObject: [
            "mentions":[
                "datnguyen"
            ]
        ])
    }
    
    //@dat!nguyen(emoji)@dato
    func testNonWordCharacters() {
        self.testParseString("@dat!nguyen(emoji)@dato", expectedJSONObject: [
            "mentions":[
                "dat",
                "dato"
            ],
            "emoticons":[
                "emoji"
            ]
        ])
    }
}
