//
//  StringDetectorKitTests.swift
//  StringDetectorKitTests
//
//  Created by Dat Nguyen on 6/25/15.
//  Copyright © 2015 VNG Corp. All rights reserved.
//

import XCTest
import StringDetectorKit

class StringDetectorKitTests: XCTestCase {
    var detector: StringDetector!
    override func setUp() {
        super.setUp()
        detector = StringDetector()
        try! detector.addHandler(EmojiHandler())
        try! detector.addHandler(MentionHandler())
        try! detector.addHandler(LinkHandler())
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        self.detector = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testParseString(string: String, expectedJSONObject: [String: [AnyObject]], timeout: NSTimeInterval = 5.0) {
        let expectation = self.expectationWithDescription("Parsing success")
        detector.parse(string) { (jsonString, jsonObject, error) -> Void in
            XCTAssertTrue(error == nil)
            XCTAssertTrue(jsonString != nil)
            XCTAssertTrue(jsonObject != nil)
            XCTAssertTrue(jsonObject == (expectedJSONObject as NSDictionary))
            expectation.fulfill()
        }
        self.waitForExpectationsWithTimeout(timeout, handler: nil)
    }
    
    func testNormalString() {
        self.testParseString("@dato (emoji) http://google.com", expectedJSONObject: [
            "links":[
                [
                    "url":"http://google.com",
                    "title":"Google"
                ]
            ],
            "mentions":[
                "dato"
            ],
            "emoticons":[
                "emoji"
            ]
        ])
    }
    
    func testEmptyResult() {
        self.testParseString("hello", expectedJSONObject: [String : [AnyObject]]())
    }
    
    func testNonASCIIChars() {
        self.testParseString("Nguyễn thành đạt (xinchào) @all", expectedJSONObject: [
            "emoticons":[
                "xinchào"
            ],
            "mentions":[
                "all"
            ]
        ])
    }
    
    func testInput1() {
        self.testParseString("@bob @john (success) such a cool feature twitter.com/jdorfman/status/430511497475670016", expectedJSONObject: [
            "mentions":[
                "bob",
                "john"
            ],
            "emoticons":[
                "success"
            ],
            "links":[
                [
                    "url": "twitter.com/jdorfman/status/430511497475670016",
                    "title": "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
                ]
            ]
        ])
    }
    
    func testPerformanceLongString() {
        // This is an example of a performance test case.
        self.measureBlock() {
            self.testParseString("This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all. This is a long string (hi) @all.",
                expectedJSONObject: [
                    "emoticons":[
                        "hi"
                    ],
                    "mentions":[
                        "all"
                    ]
            ])
        }
    }
    
}
